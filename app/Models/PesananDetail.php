<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PesananDetail extends Model
{
    use HasFactory;

    protected $table = 'pesanan_details';
    protected $fillable = ['qty', 'total_harga', 'pesanan_id', 'product_id'];

    public function pesanan(){
        return $this->belongsTo(Pesanan::class, 'pesanan_id');
    }

    public function products(){
        return $this->belongsTo(Products::class, 'product_id');
    }
}
