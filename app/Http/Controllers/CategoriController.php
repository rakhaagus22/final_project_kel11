<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categori;
use Illuminate\Support\Facades\Validator;
Use Alert;

class CategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categori = Categori::all();
        return view('data-admin.categori.index', compact('categori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('data-admin.categori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validate = $request->validate([
            'nama' => 'required|min:2|unique:categori,nama'
        ],[
            'nama.required' => 'Nama Categori Harus Diisi',
            'nama.min' => 'Nama Categori Minimal 2 karakter',
            'nama.unique' => 'Nama Categori Ini Sudah Ada'
        ]);

        Categori::create([
            'nama' => $request->nama
        ]);

        Alert::success('Berhasil', 'Success Message');
        return redirect('/categori');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validate = $request->validate([
            'nama' => 'required|min:2|unique:categori,nama'
        ],[
            'nama.required' => 'Nama Categori Harus Diisi',
            'nama.min' => 'Nama Categori Minimal 2 karakter',
            'nama.unique' => 'Nama Categori Ini Sudah Ada'
        ]);

        $categori = Categori::find($id);
        $categori->nama = $request->nama;
        $categori->save();

        Alert::success('Berhasil', 'Berhasil Update Data');
        return redirect('/categori');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $categori = Categori::find($id);
        $categori->delete();

        Alert::success('Berhasil', 'Berhasil Delete Data');
        return redirect('/categori');
    }
}
