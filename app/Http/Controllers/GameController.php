<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Game;
Use Alert;
use Illuminate\Support\Str;
use File;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $game = Game::all();
        return view('data-admin.game.index', compact('game'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('data-admin.game.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required|min:2|unique:game,nama',
            'icon' => 'required|mimes:png,jpg,jpeg,JPG,PNG,JPEG|max:3048'
        ],[
            'nama.required' => 'Nama Game Harus Diisi',
            'nama.min' => 'Nama Game Minimal 2 karakter',
            'nama.unique' => 'Nama Game Ini Sudah Ada',
            'icon.mimes' => 'Icon Hanya Menerima Format JPG, PNG, JPEG',
            'icon.required' => 'Icon Harus Ada',
            'icon.max' => 'Icon Hanya Menerima 3MB'
        ]);

        $namaFile = time().'-'.Str::slug($request->nama, '-').'.'.$request->icon->extension();

        $request->icon->move(public_path('img'), $namaFile);

        $game = new Game();

        $game->nama = $request->nama;
        $game->icon = $namaFile;

        $game->save();

        Alert::success('Berhasil', 'Berhasil Tambah Data');
        return redirect('/game');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama' => 'required|min:2|unique:game,nama',
            'icon' => 'mimes:png,jpg,jpeg,JPG,PNG,JPEG|max:3048'
        ],[
            'nama.required' => 'Nama Game Harus Diisi',
            'nama.min' => 'Nama Game Minimal 2 karakter',
            'nama.unique' => 'Nama Game Ini Sudah Ada',
            'icon.mimes' => 'Icon Hanya Menerima Format JPG, PNG, JPEG',
            'icon.max' => 'Icon Hanya Menerima 3MB'
        ]);

        $game = Game::find($id);

        if($request->has('icon')){
            $path = 'img/';
            File::delete($path. $game->icon);

            $namaFile = time().'-'.Str::slug($request->nama, '-').'.'.$request->icon->extension();
            $request->icon->move(public_path('img'), $namaFile);

            $game->icon = $namaFile;
            $game->save();
        }

        $game->nama = $request->nama;
        $game->save();

        Alert::success('Berhasil', 'Berhasil Update Data');
        return redirect('/game');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $game = Game::find($id);
        File::delete($path. $game->icon);
        $game->delete();

        Alert::success('Berhasil', 'Berhasil Delete Data');
        return redirect('/game');
    }
}
