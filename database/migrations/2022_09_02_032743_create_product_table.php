<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->unsignedBigInteger('harga');
            $table->tinyInteger('is_ready');
            $table->string('gambar');
            $table->text('deskripsi');
            $table->unsignedBigInteger('categori_id');
            $table->foreign('categori_id')->references('id')->on('categori');
            $table->unsignedBigInteger('game_id');
            $table->foreign('game_id')->references('id')->on('game')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
