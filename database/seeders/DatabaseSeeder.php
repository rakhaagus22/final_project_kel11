<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategoriSeeder::class,
            GameSeeder::class,
            ProductSeeder::class,
            ProfilSeeder::class,
            UserlSeeder::class,
        ]);
    }
}
