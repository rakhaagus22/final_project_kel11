<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('game')->insert([
        	'nama' => 'Genshin Impact',
        	'icon' => '1662330142-genshin-impact.jpg',
        ]);

        DB::table('game')->insert([
        	'nama' => 'Honkai Impact Sea',
        	'icon' => '1662330242-honkai-impact-sea.png',
        ]);

        DB::table('game')->insert([
        	'nama' => 'valorant',
        	'icon' => '1662330285-valorant.png',
        ]);

        DB::table('game')->insert([
        	'nama' => 'PUBG Mobile',
        	'icon' => '1662330354-pubg-mobile.png',
        ]);

        DB::table('game')->insert([
        	'nama' => 'Mobile Legends',
        	'icon' => '1662330485-mobile-legends.png',
        ]);
    }
}
