<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('profil')->insert([
        	'nama_lengkap' => 'Raka Agus Maulana',
        	'no_telepon' => '085155401070',
        	'alamat' => 'bBkasi',
        ]);

        DB::table('profil')->insert([
        	'nama_lengkap' => 'Gatfran',
        	'no_telepon' => '085155401070',
        	'alamat' => 'bekasi',
        ]);

        DB::table('profil')->insert([
        	'nama_lengkap' => 'Muhammad Adam Gozali',
        	'no_telepon' => '085155401070',
        	'alamat' => 'Makasar',
        ]);
    }
}
