### Final Project
---
### Kelompok 11
---
### Angota Kelompok
---
* Raka Agus Malana([@rakhaagus22](https://gitlab.com/rakhaagus22))
* Muhammad Adam Gozali([@Adamgozali](https://gitlab.com/Adamgozali))
* Ghatfan([@babydomado](https://gitlab.com/babydomado))

### Tema Project
---
**Mini E-commerce Game Top up**

### ERD
---
![Gambar teks editor VS Code](public/erd/erd.png)

### Link Vidio
---
**Link Demo Aplikasi:** [Vidio Demo](https://youtu.be/mk4IwCv6uks)\
**Link Deploy:** [project-kel11](http://project-kel11.herokuapp.com/)
