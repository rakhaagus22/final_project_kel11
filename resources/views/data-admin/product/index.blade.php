@extends('data-admin.layout.master')

@section('title')
Dashboard | Halaman Index Product
@endsection

@section('judul')
Product
@endsection

@push('script')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.12.1/datatables.min.css"/>
@endpush

@section('content')

    <a href="{{ route('product.create') }}" class="btn btn-primary btn-sm my-3">Tambah Data</a>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>#</th>
          <th>Nama Product</th>
          <th>Harga Product</th>
          <th>Redy</th>
          <th>Categori Product</th>
          <th>Game Product</th>
          <th>Gambar Product</th>
          <th>Tools</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($product as $key => $item)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $item->nama }}</td>
                <td>@currency($item->harga)</td>
                <td>
                    @if ($item->is_ready == 0)
                    <span class="badge badge-danger">Product Tidak Tersedia Sekarang</span>
                    @else
                        <span class="badge badge-primary">Product Tersedia Sekarang</span>
                    @endif
                </td>
                <td>{{ $item->categori->nama }}</td>
                <td>{{ $item->game->nama }}</td>
                <td>
                    <img src="{{ asset('/img/'.$item->gambar) }}" alt="">
                </td>
                <td>
                    <a href="{{ route('product.show', $item->id) }}" class="btn btn-info btn-sm">Show</a>
                    <a href="{{ route('product.edit', $item->id) }}" class="btn btn-warning btn-sm">Edit</a>
                    <button type="button" class="btn btn-danger text-white" data-toggle="modal" data-target="#deletemodal{{ $item->id }}">
                        Delete
                    </button>

                </td>
            </tr>
            @empty
            <h2>Data Kosong</h2>
            @endforelse
        </tbody>
    </table>
@endsection

@push('modal')
        <!-- Modal Delete-->
        <?php foreach($product as $key => $item):?>
        <div class="modal fade" id="deletemodal{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Yakin Delete Data ini? {{ $item->nama }}, data Akan Dihapus Permanen
            </div>
            <div class="modal-footer">
                <form action="{{ route('product.destroy', $item->id) }}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </div>
            </div>
        </div>
        </div>
        <?php endforeach;?>
@endpush

