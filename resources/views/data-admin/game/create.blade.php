@extends('data-admin.layout.master')

@section('title')
Dasboard | Create Data Game
@endsection

@section('judul')
Halaman Tambah Data Game
@endsection

@section('content')
<div>
        <form action="{{ route('game.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Nama Game</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" id="title" placeholder="Masukkan nama Game" value="{{ old("nama") }}">
                @error('nama')
                    <div id="validationServer03Feedback" class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group">
                <input type="file" name="icon">
                @error('icon')
                    <div class="alert alert-danger my-3">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection


